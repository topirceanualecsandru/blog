from django import forms

from blog_app.models import *


class ArticleForm(forms.ModelForm):  # ModelForm folosim cand vrem sa generam un formular pe baza unui model
    class Meta:  # in interiorul class Meta se pun specificatiile modelului si field-urilor formularului
        model = Article
        fields = '__all__'  # o lista cu toate field-urilor modelului
        # fields = ['title', 'content', 'picture']  # o lista doar field-urile necesare din model
        exclude = ['datetime']  # excludem ce field-uri nu vrem sa apara in formular
        widgets = {'title': forms.TextInput(attrs={'class': 'form-control', 'style': 'background-color:red;'})}


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ['name_surname', 'telephone', 'email', 'content']


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['accepted']


class InlineCommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = '__all__'
