from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone


class Article(models.Model):
    title = models.CharField(max_length=75)
    content = models.TextField()
    picture = models.ImageField(upload_to="static/images/")
    datetime = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.title


class Comment(models.Model):
    article = models.ForeignKey(Article, related_name='article', on_delete=models.CASCADE)
    parent_comment = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)
    author_name = models.CharField(max_length=50)
    content = models.TextField()
    datetime = models.DateTimeField(auto_now=True)
    accepted = models.BooleanField(default=False)

    class Meta:
        ordering = ['-datetime']

    def __str__(self):
        return f"{self.author_name} - {self.content}"


def telephone_valid(value):
    valid_characters = '0123456789-().'
    for character in value:
        if character not in valid_characters:
            raise ValidationError(f"{character} is not a valid character for phone number (accepted only '-().')")
    if value[:2] != "07":
        raise ValidationError(f"{value} is not a Romanian phone number")
    numeric_phone = list(
        filter(lambda character: character not in '-().', value))  # scoate caracterele care nu sunt cifre din value
    print(f"{numeric_phone}")
    if len(numeric_phone) != 10:
        raise ValidationError(f"{value} length is invalid")


class Contact(models.Model):
    name_surname = models.CharField(max_length=50, default="")
    telephone = models.CharField(max_length=18, default="0", validators=[
        telephone_valid])  # !!! NU  punem telephone_valid() pentru ca referim functia in loc sa o apelam
    email = models.EmailField(default="nimic@chiarnimic.wow")
    content = models.TextField()
    datetime = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name_surname
