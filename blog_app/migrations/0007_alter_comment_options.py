# Generated by Django 3.2.7 on 2021-09-30 19:07

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog_app', '0006_alter_comment_options'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='comment',
            options={'ordering': ['-datetime']},
        ),
    ]
