from django.shortcuts import render
from django.views.generic import *

from blog_app.forms import *
from blog_app.models import *


class HomepageView(
    ListView):  # inlocuitor pentru home_page_view; ListView scoate o lista din modelul respectiv si o trimite
    # spre template_name pe cheia context_object_name
    template_name = 'homepage.html'  # numele template-ului care se incarca
    model = Article  # modelul cu care lucreaza view-ul
    context_object_name = 'articles'  # numele cheii din dictionarul de context


class AdminPageView(TemplateView):
    template_name = 'admin_page.html'


class ArticlesView(ListView):
    template_name = 'articles_view.html'
    model = Article
    context_object_name = 'articles'


class CommentsView(ListView):
    template_name = 'comments_view.html'
    model = Comment
    context_object_name = 'comments'


def home_page_view(request):
    all_articles = Article.objects.all()
    last_articles = Article.objects.all().order_by('-id')[:3]
    return render(request, 'homepage.html', {'articles': all_articles, 'last_articles': last_articles})


# class ArticleDetailView(DetailView):  # DetailView scoate o bucata din modelul respectiv si trimite
#     # spre template_name pe cheia context_object_name (la fel ca si cel de mai sus)
#     model = Article
#     template_name = 'article.html'
#     context_object_name = 'article'
#
#     def get_context_data(self, **kwargs):
#         context: dict = super().get_context_data(
#             **kwargs)  # apelam get_context_data al lui DetailView; obtinem dictionarul {"article":...}
#         article = context[self.context_object_name]  # scoatem din dictionarul de mai sus valoarea
#         context['comments'] = Comment.objects.filter(article=article)  # adaugam in dictionarul de context comentariile
#         return context

def get_article_comments(article):
    parents = Comment.objects.filter(article=article, parent_comment=None)
    parents_with_replies = get_comments_with_children(parents)
    return parents_with_replies


def get_comments_with_children(comments):
    parents_with_replies = list(map(
        lambda comment: {'comment': comment,
                         'children': get_comments_with_children(Comment.objects.filter(parent_comment=comment))},
        comments))
    return parents_with_replies


def article_detail_view(request, article_id):
    if request.method == 'POST':
        comment_form = InlineCommentForm(data=request.POST)
        if comment_form.is_valid():
            comment_form.save()
    article = Article.objects.get(id=article_id)
    all_comments = get_article_comments(article)
    # print(all_comments)
    # comment = Comment()
    # if request.method == 'POST':
    #     if request.POST.get('author_name') and request.POST.get('content'):
    #         comment.article = article
    #         comment.author_name = request.POST.get('author_name')
    #         comment.content = request.POST.get('content')
    #         comment.save()

    return render(request, 'article.html',
                  {'article': article, 'comments': all_comments})  # al treilea parametru e dictionarul de context


class ArticleCreateView(CreateView):  # CreateView instantiaza un formular si il trimite spre template
    model = Article  # model = modelul pentru care face create
    # (modelul cu care lucreaa respectivul view - valabil pentru majoritatea view-urilor)
    # fields = '__all__'
    form_class = ArticleForm  # clasa din care face parte formularul
    # context_object_name = 'article_form'
    template_name = 'article_create.html'  # numele template-ului care se incarca
    success_url = '/admin_page'  # url-ul la care ne transfera in momentul in care s-a facut create cu succes.
    # Daca e failure ramane activ formularul pentru a corecta greselile + mesaje de eroare etc (in urmatorul curs)


class ContactCreateView(CreateView):
    model = Contact
    form_class = ContactForm
    template_name = 'contact_create.html'
    success_url = '/'


class ArticleUpdateView(UpdateView):
    # primeste un model, creeaza un formular si incarca un template in care editam respectivul articol
    model = Article
    form_class = ArticleForm
    template_name = 'article_update.html'
    success_url = '/admin_page'
    context_object_name = 'article'  # il utilizam cand contruim form-ul ca in html
    # in general face override la cheia din dictionarul de context


class CommentUpdateView(UpdateView):
    # primeste un model, creeaza un formular si incarca un template in care editam respectivul articol
    model = Comment
    form_class = CommentForm
    template_name = 'coment_update.html'
    success_url = '/comments_view'


class ArticleDeleteView(DeleteView):
    # primeste un model si incarca un template in care confirmam stergerea respectivului articol
    model = Article
    template_name = 'article_delete.html'
    success_url = '/admin_page'

    def get_context_data(self,
                         **kwargs):  # pt ca delete view nu ne da obiectul in template, trebuie sa o implementam manual
        context: dict = super().get_context_data(
            **kwargs)
        article = self.object  # pe proprietatea object isi tine obiectul curent update, delete si detail view
        context['article'] = article  # pasam articolul pe cheia article
        return context

# def listare_categorii(comments, parent_comment=None, curent_level=1) :
#     'afiseaza numele categoriilor pe nivele de indentare'
#
#     spaces = "    "
#     for comentariu in comments :
#         if comentariu.get("parent_comment", None) == Comment.parent_comment :
#             print(f"{spaces * curent_level}{comentariu['user']}{comentariu['content']}")
#             listare_categorii(comments, comentariu['id'], curent_level + 1)
