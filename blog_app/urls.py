from django.urls import path

from blog_app import views

urlpatterns = [
    path('', views.home_page_view, name='home'),
    # path('', views.HomepageView.as_view(), name='home'),  # cand legam o clasa trebuie sa apelam .as_view() pe ea

    path('article/<int:article_id>/', views.article_detail_view, name='article_detail'),
    # path('article/<int:pk>/', views.ArticleDetailView.as_view(), name='article_detail'),

    path('article_create/', views.ArticleCreateView.as_view(), name='article_create'),
    # article_create - nu are nevoie de alti parametri in url
    # path ('sufixul din url/', view-ul cu care sa se lege, un nume unic pentru care sa generam usor url-ul cu ajutorul lui {% url 'numele url-ului' %})

    path('article_update/<int:pk>/', views.ArticleUpdateView.as_view(), name='article_update'),
    # avem nevoie de <int:pk>/ pentru ca operam pe un anumit obiect

    path('article_delete/<int:pk>/', views.ArticleDeleteView.as_view(), name='article_delete'),
    # avem nevoie de <int:pk>/ pentru ca operam pe un anumit obiect

    path('contact_create/', views.ContactCreateView.as_view(), name='contact_create'),

    path('admin_page/', views.AdminPageView.as_view(), name='admin_page'),

    path('articles_view/', views.ArticlesView.as_view(), name='articles_view'),

    path('comments_view/', views.CommentsView.as_view(), name='comments_view'),

    path('comment_update/<int:pk>/', views.CommentUpdateView.as_view(), name='comment_update'),

]
