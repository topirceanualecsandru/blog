from django.contrib import admin

# Register your models here.
from blog_app.models import *

admin.site.register(Article)
admin.site.register(Comment)
admin.site.register(Contact)
